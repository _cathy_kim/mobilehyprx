/*
 * Copyright 2019 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tensorflow.lite.examples.hyprX.lib

import android.content.Context
import android.graphics.Bitmap
import android.os.SystemClock
import android.util.Log
import org.tensorflow.lite.Interpreter
import org.tensorflow.lite.gpu.GpuDelegate
import java.io.FileInputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import kotlin.math.exp


class Position {
  var x: Float = 0.0f
  var y: Float = 0.0f
}

class Face {
  var landmarks = listOf<Position>()
  var score: Float = 0.0f
}


enum class Device {
  CPU,
  NNAPI,
  GPU
}

class HyprX(
  val context: Context,
  val filename: String = "hyprXLiter_change_output_fp32_v34.tflite",
  //"hyprXLiter_change_output_default_v3.tflite",
  //"hyprXLiter_v2_fp32.tflite",
  // "hyprXLiter_change_output_fp32_v2.tflite",
  // "hyprXLiter_origin.tflite",
  //"hyprXLiter_v2_fp32.tflite",
  // "hyprXLiter_origin.tflite",
  // "hgnetXLiter_v1_fp32.tflite",
  // "hyprXLiter_origin.tflite",
  // "hgnetXLiter_v1_fp32.tflite",
  // "hyprXLiter_newShape.tflite",
  // "hyprXLiter_fp16.tflite",
  // "hyprXLiter_fp16.tflite",
  //"hgnetXLiter_v1_fp32.tflite",
  // "hgnet_model_fp32.tflite",
  // val device: Device = Device.CPU

  val device: Device = Device.GPU
  //val device: Device = Device.NNAPI

) : AutoCloseable {
  var lastInferenceTimeNanos: Long = -1
    private set

  /** An Interpreter for the TFLite model.   */
  private var interpreter: Interpreter? = null
  private var gpuDelegate: GpuDelegate? = null
  private val NUM_LITE_THREADS = 4

  private fun getInterpreter(): Interpreter {
    if (interpreter != null) {
      return interpreter!!
    }
    val options = Interpreter.Options()
    options.setNumThreads(NUM_LITE_THREADS)
    when (device) {
      Device.CPU -> { }
      Device.GPU -> {
        gpuDelegate = GpuDelegate()
        options.addDelegate(gpuDelegate)
      }
      Device.NNAPI -> options.setUseNNAPI(true)
    }
    interpreter = Interpreter(loadModelFile(filename, context), options)
    return interpreter!!
  }

  override fun close() {
    interpreter?.close()
    interpreter = null
    gpuDelegate?.close()
    gpuDelegate = null
  }

  /** Returns value within [0,1].   */
  private fun sigmoid(x: Float): Float {
    return (1.0f / (1.0f + exp(-x)))
  }

  /**
   * Scale the image to a byteBuffer of [-1,1] values.
   */
  private fun initInputArray(bitmap: Bitmap): ByteBuffer {
    val bytesPerChannel = 4
    val inputChannels = 3
    val batchSize = 1
    val inputBuffer = ByteBuffer.allocateDirect(
      batchSize * bytesPerChannel * bitmap.height * bitmap.width * inputChannels
    )
    inputBuffer.order(ByteOrder.nativeOrder())

    inputBuffer.rewind()

    val mean = 128.0f
    val std = 128.0f
    val intValues = IntArray(bitmap.width * bitmap.height)
    bitmap.getPixels(intValues, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
    for (pixelValue in intValues) {
      inputBuffer.putFloat(((pixelValue shr 16 and 0xFF) - mean) / std)
      inputBuffer.putFloat(((pixelValue shr 8 and 0xFF) - mean) / std)
      inputBuffer.putFloat(((pixelValue and 0xFF) - mean) / std)
    }
    return inputBuffer
  }

  /** Preload and memory map the model file, returning a MappedByteBuffer containing the model. */
  private fun loadModelFile(path: String, context: Context): MappedByteBuffer {
    val fileDescriptor = context.assets.openFd(path)
    val inputStream = FileInputStream(fileDescriptor.fileDescriptor)
    return inputStream.channel.map(
      FileChannel.MapMode.READ_ONLY, fileDescriptor.startOffset, fileDescriptor.declaredLength
    )
  }

  /**
   * Initializes an outputMap of 1 * x * y * z FloatArrays for the model processing to populate.
   */
  private fun initOutputMap(interpreter: Interpreter): HashMap<Int, Any> {
    val outputMap = HashMap<Int, Any>()

//@  For 1,131,2
    val landmarksShape = interpreter.getOutputTensor(0).shape()
    println(landmarksShape) // 1, 131, 2
    outputMap[0] = Array(landmarksShape[0]){
      Array(landmarksShape[1]){
        FloatArray(landmarksShape[2])
      }
    }
//For 131,2
//    val landmarksShape = interpreter.getOutputTensor(0).shape()
//    println(landmarksShape) // 131, 2 -> 1,131,2
//    outputMap[0] = Array(landmarksShape[0]) {
//        FloatArray(landmarksShape[1])
//      }



    val scoreShape = interpreter.getOutputTensor(1).shape()
    println(scoreShape)
    outputMap[1] = Array(scoreShape[0]){
      FloatArray(scoreShape[1])
    }

    return outputMap
  }





  /**
   * Estimates the pose for a single person.
   * args:
   *      bitmap: image bitmap of frame that should be processed
   * returns:
   *      person: a Person object containing data about keypoint locations and confidence scores
   */
  @Suppress("UNCHECKED_CAST")
  fun estimateSinglePose(bitmap: Bitmap): Face {
    val estimationStartTimeNanos = SystemClock.elapsedRealtimeNanos()
    val inputArray = arrayOf(initInputArray(bitmap))
    Log.i(
      "hyprX",
      String.format(
        "Scaling to [-1,1] took %.2f ms",
        1.0f * (SystemClock.elapsedRealtimeNanos() - estimationStartTimeNanos) / 1_000_000
      )
    )

    val outputMap = initOutputMap(getInterpreter())

    val inferenceStartTimeNanos = SystemClock.elapsedRealtimeNanos()
    getInterpreter().runForMultipleInputsOutputs(inputArray, outputMap)
    lastInferenceTimeNanos = SystemClock.elapsedRealtimeNanos() - inferenceStartTimeNanos
    Log.i(
      "hyprX",
      String.format("Interpreter took %.2f ms", 1.0f * lastInferenceTimeNanos / 1_000_000)
    )


    val landmarkList = outputMap[0] as Array<Array<FloatArray>> //1,@131,2
//    val landmarkList = outputMap[0] as Array<FloatArray> //@131,2

    val faceScore = outputMap[1] as Array<FloatArray>

    val face = Face();
    val landmark = Array(131) { Position() }
    var totalScore = 0.0f
    //println(landmarkList[0][0][1])
    //@1,131,2
 
    println("start")
    landmarkList[0].forEachIndexed { index, floats ->
      val landmarkX = landmarkList[0][index][0]
      val landmarkY = landmarkList[0][index][1]

      val pos = Position()
      pos.x = landmarkX
      pos.y = landmarkY
      println(""+pos.x+","+pos.y)

      landmark[index] = pos
    }
    println("end")
    //For 131,2
//    landmarkList.forEachIndexed { index, floats ->
//      val landmarkX = landmarkList[index][0]
//      val landmarkY = landmarkList[index][1]
//
//      val pos = Position()
//      pos.x = landmarkX
//      pos.y = landmarkY
//      landmark[index] = pos
//    }

    face.landmarks = landmark.toList()
    face.score = faceScore[0][0]

    return face;
  }
}
